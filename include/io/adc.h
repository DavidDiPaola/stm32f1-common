/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#ifndef _COMMON_IO_ADC_H
#define _COMMON_IO_ADC_H

#include <feature/adc.h>

/* clock source: SYSCLK --> AHB -- > APB2 (72Mhz max) --> ADC prescaler --> ADC (14Mhz max) */ /* RM0008 figure 8, figure 11 */

/* ADC channel pins:
	ADC1
		TODO other ADC1 channels
		16: internal temperature sensor
		17: internal voltage reference
	ADC2
		TODO other ADC2 channels
		16,17: Vss
	ADC3
		TODO other ADC3 channels
		9,14,15,16,17: Vss
*/ /* RM0008 11.12.4, 11.12.5 */

/* RM0008 11.12 */
struct __attribute__ ((__packed__)) adc {
	/* 0x00 */  volatile u32 SR;     /* Status Register, RM0008 11.12.1 */
	/* 0x04 */  volatile u32 CR1;    /* Control Register 1, RM0008 11.12.2 */
	/* 0x08 */  volatile u32 CR2;    /* Control Register 2, RM0008 11.12.3 */
	/* 0x0C */  volatile u32 SMPR1;  /* SaMPle time Register 1, RM0008 11.12.4 */
	/* 0x10 */  volatile u32 SMPR2;  /* SaMPle time Register 2, RM0008 11.12.5 */
	/* 0x14 */  volatile u32 JOFR1;  /* inJected channel data OFfset Register 1, RM0008 11.12.6 */
	/* 0x18 */  volatile u32 JOFR2;  /* inJected channel data OFfset Register 2, RM0008 11.12.6 */
	/* 0x1C */  volatile u32 JOFR3;  /* inJected channel data OFfset Register 3, RM0008 11.12.6 */
	/* 0x20 */  volatile u32 JOFR4;  /* inJected channel data OFfset Register 4, RM0008 11.12.6 */
	/* 0x24 */  volatile u32 HTR;    /* watchdog High Threshold Register, RM0008 11.12.7 */
	/* 0x28 */  volatile u32 LTR;    /* watchdog Low Threshold Register, RM0008 11.12.8 */
	/* 0x2C */  volatile u32 SQR1;   /* regular SeQuence Register 1, RM0008 11.12.9 */
	/* 0x30 */  volatile u32 SQR2;   /* regular SeQuence Register 2, RM0008 11.12.10 */
	/* 0x34 */  volatile u32 SQR3;   /* regular SeQuence Register 3, RM0008 11.12.11 */
	/* 0x38 */  volatile u32 JSQR;   /* inJected SeQuence Register, RM0008 11.12.12 */
	/* 0x3C */  volatile u32 JDR1;   /* inJected Data Register 1, RM0008 11.12.13 */
	/* 0x40 */  volatile u32 JDR2;   /* inJected Data Register 2, RM0008 11.12.13 */
	/* 0x44 */  volatile u32 JDR3;   /* inJected Data Register 3, RM0008 11.12.13 */
	/* 0x48 */  volatile u32 JDR4;   /* inJected Data Register 4, RM0008 11.12.13 */
	/* 0x4C */  volatile u32 DR;     /* regular Data Register, RM0008 11.12.14 */
};

/* RM0008 11.12.1 */
static const u32 adc_SR_STRT  = (1 << 4);  /* regular channel STaRT flag */
static const u32 adc_SR_JSTRT = (1 << 3);  /* inJected channel STaRT flag */
static const u32 adc_SR_JEOC  = (1 << 2);  /* inJected channel End Of Conversion flag */
static const u32 adc_SR_EOC   = (1 << 1);  /* End Of Conversion flag */
static const u32 adc_SR_AWD   = (1 << 0);  /* Analog WatchDog flag */

/* RM0008 11.12.2 */
static const u32 adc_CR1_AWDEN        = (      1 << 23);  /* regular channel Analog WatchDog ENable */
static const u32 adc_CR1_JAWDEN       = (      1 << 22);  /* inJected channel Analog WatchDog ENable */
static const u32 adc_CR1_DUALMOD_MASK = ( 0b1111 << 16);
static const u32 adc_CR1_DUALMOD_INDP = ( 0b0000 << 16);  /* DUAL MODe: INDePendant mode */
/* TODO other CR1 DUALMOD modes */
static const u32 adc_CR1_DISCNUM_MASK = (  0b111 << 13);
static const u32 adc_CR1_DISCNUM_1CH  = (  0b000 << 13);  /* DISContinuous mode NUMber of channels: 1 CHannel */
/* TODO other CR1 DISCNUM modes */
static const u32 adc_CR1_JDISCEN      = (      1 << 12);  /* inJected channels DIScontinuous mode ENable */
static const u32 adc_CR1_DISCEN       = (      1 << 11);  /* regular channels DIScontinuous mode ENable */
static const u32 adc_CR1_JAUTO        = (      1 << 10);  /* inJected channels AUTOmatic conversion enable */
static const u32 adc_CR1_AWDSGL       = (      1 <<  9);  /* Analog WatchDog for SinGLe channel in scan mode enable */
static const u32 adc_CR1_SCAN         = (      1 <<  8);  /* SCAN mode enable */
static const u32 adc_CR1_JEOCIE       = (      1 <<  7);  /* inJected channels End Of Conversion Interrupt Enable */
static const u32 adc_CR1_AWDIE        = (      1 <<  6);  /* Analog WatchDog Interrupt Enable */
static const u32 adc_CR1_EOCIE        = (      1 <<  5);  /* regular channels End Of Conversion Interrupt Enable */
static const u32 adc_CR1_AWDCH_MASK   = (0b11111 <<  0);
static const u32 adc_CR1_AWDCH_CH0    = (0b00000 <<  0);  /* Analog WatchDog CHannel select: CHannel 0 */
/* TODO other CR1 AWDCH channels */

/* RM0008 11.12.3 */
static const u32 adc_CR2_TSVREFE          = (    1 << 23);  /* Temperature Sensor and Voltage REFerence (internal) Enable */
static const u32 adc_CR2_SWSTART          = (    1 << 22);  /* ?SW? START */
static const u32 adc_CR2_JSWSTART         = (    1 << 21);  /* inJected ?SW? START */
static const u32 adc_CR2_EXTTRIG          = (    1 << 20);  /* regular channel conversion EXTernal TRIGger enable */
static const u32 adc_CR2_EXTSEL_MASK      = (0b111 << 17);
/* TODO other CR2 EXTSEL sources */
static const u32 adc_CR2_EXTSEL_SWSTART   = (0b111 << 17);  /* regular channel EXTernal event SELect for ADC1, ADC2, ADC3: SWSTART */
static const u32 adc_CR2_JEXTTRIG         = (    1 << 15);  /* inJected channel conversion EXTernal TRIGger enable */
static const u32 adc_CR2_JEXTSEL_MASK     = (0b111 << 12);
/* TODO other CR2 JEXTSEL sources */
static const u32 adc_CR2_JEXTSEL_JSWSTART = (0b111 << 12);  /* inJected regular channel EXTernal event SELect for ADC1, ADC2, ADC3: JSWSTART */
static const u32 adc_CR2_ALIGN_MASK       = (    1 << 11);
static const u32 adc_CR2_ALIGN_RIGHT      = (    0 << 11);  /* data ALIGNment: right */
static const u32 adc_CR2_ALIGN_LEFT       = (    1 << 11);  /* data ALIGNment: left */
static const u32 adc_CR2_DMA              = (    1 <<  8);  /* DMA mode enable */
static const u32 adc_CR2_RSTCAL           = (    1 <<  3);  /* ReSeT CALibration */
static const u32 adc_CR2_CAL              = (    1 <<  2);  /* start CALibration */
static const u32 adc_CR2_CONT             = (    1 <<  1);  /* CONTinuous conversion mode enable */
static const u32 adc_CR2_ADON             = (    1 <<  0);  /* ADc ON */

/* RM0008 11.12.4 */
static const u32 adc_SMPR1_SMP17_MASK = (0b111 << 21);
static const u32 adc_SMPR1_SMP17_1    = (0b000 << 21);  /* channel 17 sample time: 1.5 cycles */
static const u32 adc_SMPR1_SMP17_7    = (0b001 << 21);  /* channel 17 sample time: 7.5 cycles */
static const u32 adc_SMPR1_SMP17_13   = (0b010 << 21);  /* channel 17 sample time: 13.5 cycles */
static const u32 adc_SMPR1_SMP17_28   = (0b011 << 21);  /* channel 17 sample time: 28.5 cycles */
static const u32 adc_SMPR1_SMP17_41   = (0b100 << 21);  /* channel 17 sample time: 41.5 cycles */
static const u32 adc_SMPR1_SMP17_55   = (0b101 << 21);  /* channel 17 sample time: 55.5 cycles */
static const u32 adc_SMPR1_SMP17_71   = (0b110 << 21);  /* channel 17 sample time: 71.5 cycles */
static const u32 adc_SMPR1_SMP17_239  = (0b111 << 21);  /* channel 17 sample time: 239.5 cycles */
static const u32 adc_SMPR1_SMP16_MASK = (0b111 << 18);
static const u32 adc_SMPR1_SMP16_1    = (0b000 << 18);  /* channel 16 sample time: 1.5 cycles */
static const u32 adc_SMPR1_SMP16_7    = (0b001 << 18);  /* channel 16 sample time: 7.5 cycles */
static const u32 adc_SMPR1_SMP16_13   = (0b010 << 18);  /* channel 16 sample time: 13.5 cycles */
static const u32 adc_SMPR1_SMP16_28   = (0b011 << 18);  /* channel 16 sample time: 28.5 cycles */
static const u32 adc_SMPR1_SMP16_41   = (0b100 << 18);  /* channel 16 sample time: 41.5 cycles */
static const u32 adc_SMPR1_SMP16_55   = (0b101 << 18);  /* channel 16 sample time: 55.5 cycles */
static const u32 adc_SMPR1_SMP16_71   = (0b110 << 18);  /* channel 16 sample time: 71.5 cycles */
static const u32 adc_SMPR1_SMP16_239  = (0b111 << 18);  /* channel 16 sample time: 239.5 cycles */
static const u32 adc_SMPR1_SMP15_MASK = (0b111 << 15);
static const u32 adc_SMPR1_SMP15_1    = (0b000 << 15);  /* channel 15 sample time: 1.5 cycles */
static const u32 adc_SMPR1_SMP15_7    = (0b001 << 15);  /* channel 15 sample time: 7.5 cycles */
static const u32 adc_SMPR1_SMP15_13   = (0b010 << 15);  /* channel 15 sample time: 13.5 cycles */
static const u32 adc_SMPR1_SMP15_28   = (0b011 << 15);  /* channel 15 sample time: 28.5 cycles */
static const u32 adc_SMPR1_SMP15_41   = (0b100 << 15);  /* channel 15 sample time: 41.5 cycles */
static const u32 adc_SMPR1_SMP15_55   = (0b101 << 15);  /* channel 15 sample time: 55.5 cycles */
static const u32 adc_SMPR1_SMP15_71   = (0b110 << 15);  /* channel 15 sample time: 71.5 cycles */
static const u32 adc_SMPR1_SMP15_239  = (0b111 << 15);  /* channel 15 sample time: 239.5 cycles */
static const u32 adc_SMPR1_SMP14_MASK = (0b111 << 12);
static const u32 adc_SMPR1_SMP14_1    = (0b000 << 12);  /* channel 14 sample time: 1.5 cycles */
static const u32 adc_SMPR1_SMP14_7    = (0b001 << 12);  /* channel 14 sample time: 7.5 cycles */
static const u32 adc_SMPR1_SMP14_13   = (0b010 << 12);  /* channel 14 sample time: 13.5 cycles */
static const u32 adc_SMPR1_SMP14_28   = (0b011 << 12);  /* channel 14 sample time: 28.5 cycles */
static const u32 adc_SMPR1_SMP14_41   = (0b100 << 12);  /* channel 14 sample time: 41.5 cycles */
static const u32 adc_SMPR1_SMP14_55   = (0b101 << 12);  /* channel 14 sample time: 55.5 cycles */
static const u32 adc_SMPR1_SMP14_71   = (0b110 << 12);  /* channel 14 sample time: 71.5 cycles */
static const u32 adc_SMPR1_SMP14_239  = (0b111 << 12);  /* channel 14 sample time: 239.5 cycles */
static const u32 adc_SMPR1_SMP13_MASK = (0b111 << 9);
static const u32 adc_SMPR1_SMP13_1    = (0b000 << 9);  /* channel 13 sample time: 1.5 cycles */
static const u32 adc_SMPR1_SMP13_7    = (0b001 << 9);  /* channel 13 sample time: 7.5 cycles */
static const u32 adc_SMPR1_SMP13_13   = (0b010 << 9);  /* channel 13 sample time: 13.5 cycles */
static const u32 adc_SMPR1_SMP13_28   = (0b011 << 9);  /* channel 13 sample time: 28.5 cycles */
static const u32 adc_SMPR1_SMP13_41   = (0b100 << 9);  /* channel 13 sample time: 41.5 cycles */
static const u32 adc_SMPR1_SMP13_55   = (0b101 << 9);  /* channel 13 sample time: 55.5 cycles */
static const u32 adc_SMPR1_SMP13_71   = (0b110 << 9);  /* channel 13 sample time: 71.5 cycles */
static const u32 adc_SMPR1_SMP13_239  = (0b111 << 9);  /* channel 13 sample time: 239.5 cycles */
static const u32 adc_SMPR1_SMP12_MASK = (0b111 << 6);
static const u32 adc_SMPR1_SMP12_1    = (0b000 << 6);  /* channel 12 sample time: 1.5 cycles */
static const u32 adc_SMPR1_SMP12_7    = (0b001 << 6);  /* channel 12 sample time: 7.5 cycles */
static const u32 adc_SMPR1_SMP12_13   = (0b010 << 6);  /* channel 12 sample time: 13.5 cycles */
static const u32 adc_SMPR1_SMP12_28   = (0b011 << 6);  /* channel 12 sample time: 28.5 cycles */
static const u32 adc_SMPR1_SMP12_41   = (0b100 << 6);  /* channel 12 sample time: 41.5 cycles */
static const u32 adc_SMPR1_SMP12_55   = (0b101 << 6);  /* channel 12 sample time: 55.5 cycles */
static const u32 adc_SMPR1_SMP12_71   = (0b110 << 6);  /* channel 12 sample time: 71.5 cycles */
static const u32 adc_SMPR1_SMP12_239  = (0b111 << 6);  /* channel 12 sample time: 239.5 cycles */
static const u32 adc_SMPR1_SMP11_MASK = (0b111 << 3);
static const u32 adc_SMPR1_SMP11_1    = (0b000 << 3);  /* channel 11 sample time: 1.5 cycles */
static const u32 adc_SMPR1_SMP11_7    = (0b001 << 3);  /* channel 11 sample time: 7.5 cycles */
static const u32 adc_SMPR1_SMP11_13   = (0b010 << 3);  /* channel 11 sample time: 13.5 cycles */
static const u32 adc_SMPR1_SMP11_28   = (0b011 << 3);  /* channel 11 sample time: 28.5 cycles */
static const u32 adc_SMPR1_SMP11_41   = (0b100 << 3);  /* channel 11 sample time: 41.5 cycles */
static const u32 adc_SMPR1_SMP11_55   = (0b101 << 3);  /* channel 11 sample time: 55.5 cycles */
static const u32 adc_SMPR1_SMP11_71   = (0b110 << 3);  /* channel 11 sample time: 71.5 cycles */
static const u32 adc_SMPR1_SMP11_239  = (0b111 << 3);  /* channel 11 sample time: 239.5 cycles */
static const u32 adc_SMPR1_SMP10_MASK = (0b111 << 0);
static const u32 adc_SMPR1_SMP10_1    = (0b000 << 0);  /* channel 10 sample time: 1.5 cycles */
static const u32 adc_SMPR1_SMP10_7    = (0b001 << 0);  /* channel 10 sample time: 7.5 cycles */
static const u32 adc_SMPR1_SMP10_13   = (0b010 << 0);  /* channel 10 sample time: 13.5 cycles */
static const u32 adc_SMPR1_SMP10_28   = (0b011 << 0);  /* channel 10 sample time: 28.5 cycles */
static const u32 adc_SMPR1_SMP10_41   = (0b100 << 0);  /* channel 10 sample time: 41.5 cycles */
static const u32 adc_SMPR1_SMP10_55   = (0b101 << 0);  /* channel 10 sample time: 55.5 cycles */
static const u32 adc_SMPR1_SMP10_71   = (0b110 << 0);  /* channel 10 sample time: 71.5 cycles */
static const u32 adc_SMPR1_SMP10_239  = (0b111 << 0);  /* channel 10 sample time: 239.5 cycles */

/* RM0008 11.12.5 */
static const u32 adc_SMPR2_SMP9_MASK = (0b111 << 27);
static const u32 adc_SMPR2_SMP9_1    = (0b000 << 27);  /* channel 9 sample time: 1.5 cycles */
static const u32 adc_SMPR2_SMP9_7    = (0b001 << 27);  /* channel 9 sample time: 7.5 cycles */
static const u32 adc_SMPR2_SMP9_13   = (0b010 << 27);  /* channel 9 sample time: 13.5 cycles */
static const u32 adc_SMPR2_SMP9_28   = (0b011 << 27);  /* channel 9 sample time: 28.5 cycles */
static const u32 adc_SMPR2_SMP9_41   = (0b100 << 27);  /* channel 9 sample time: 41.5 cycles */
static const u32 adc_SMPR2_SMP9_55   = (0b101 << 27);  /* channel 9 sample time: 55.5 cycles */
static const u32 adc_SMPR2_SMP9_71   = (0b110 << 27);  /* channel 9 sample time: 71.5 cycles */
static const u32 adc_SMPR2_SMP9_239  = (0b111 << 27);  /* channel 9 sample time: 239.5 cycles */
static const u32 adc_SMPR2_SMP8_MASK = (0b111 << 24);
static const u32 adc_SMPR2_SMP8_1    = (0b000 << 24);  /* channel 8 sample time: 1.5 cycles */
static const u32 adc_SMPR2_SMP8_7    = (0b001 << 24);  /* channel 8 sample time: 7.5 cycles */
static const u32 adc_SMPR2_SMP8_13   = (0b010 << 24);  /* channel 8 sample time: 13.5 cycles */
static const u32 adc_SMPR2_SMP8_28   = (0b011 << 24);  /* channel 8 sample time: 28.5 cycles */
static const u32 adc_SMPR2_SMP8_41   = (0b100 << 24);  /* channel 8 sample time: 41.5 cycles */
static const u32 adc_SMPR2_SMP8_55   = (0b101 << 24);  /* channel 8 sample time: 55.5 cycles */
static const u32 adc_SMPR2_SMP8_71   = (0b110 << 24);  /* channel 8 sample time: 71.5 cycles */
static const u32 adc_SMPR2_SMP8_239  = (0b111 << 24);  /* channel 8 sample time: 239.5 cycles */
static const u32 adc_SMPR2_SMP7_MASK = (0b111 << 21);
static const u32 adc_SMPR2_SMP7_1    = (0b000 << 21);  /* channel 7 sample time: 1.5 cycles */
static const u32 adc_SMPR2_SMP7_7    = (0b001 << 21);  /* channel 7 sample time: 7.5 cycles */
static const u32 adc_SMPR2_SMP7_13   = (0b010 << 21);  /* channel 7 sample time: 13.5 cycles */
static const u32 adc_SMPR2_SMP7_28   = (0b011 << 21);  /* channel 7 sample time: 28.5 cycles */
static const u32 adc_SMPR2_SMP7_41   = (0b100 << 21);  /* channel 7 sample time: 41.5 cycles */
static const u32 adc_SMPR2_SMP7_55   = (0b101 << 21);  /* channel 7 sample time: 55.5 cycles */
static const u32 adc_SMPR2_SMP7_71   = (0b110 << 21);  /* channel 7 sample time: 71.5 cycles */
static const u32 adc_SMPR2_SMP7_239  = (0b111 << 21);  /* channel 7 sample time: 239.5 cycles */
static const u32 adc_SMPR2_SMP6_MASK = (0b111 << 18);
static const u32 adc_SMPR2_SMP6_1    = (0b000 << 18);  /* channel 6 sample time: 1.5 cycles */
static const u32 adc_SMPR2_SMP6_7    = (0b001 << 18);  /* channel 6 sample time: 7.5 cycles */
static const u32 adc_SMPR2_SMP6_13   = (0b010 << 18);  /* channel 6 sample time: 13.5 cycles */
static const u32 adc_SMPR2_SMP6_28   = (0b011 << 18);  /* channel 6 sample time: 28.5 cycles */
static const u32 adc_SMPR2_SMP6_41   = (0b100 << 18);  /* channel 6 sample time: 41.5 cycles */
static const u32 adc_SMPR2_SMP6_55   = (0b101 << 18);  /* channel 6 sample time: 55.5 cycles */
static const u32 adc_SMPR2_SMP6_71   = (0b110 << 18);  /* channel 6 sample time: 71.5 cycles */
static const u32 adc_SMPR2_SMP6_239  = (0b111 << 18);  /* channel 6 sample time: 239.5 cycles */
static const u32 adc_SMPR2_SMP5_MASK = (0b111 << 15);
static const u32 adc_SMPR2_SMP5_1    = (0b000 << 15);  /* channel 5 sample time: 1.5 cycles */
static const u32 adc_SMPR2_SMP5_7    = (0b001 << 15);  /* channel 5 sample time: 7.5 cycles */
static const u32 adc_SMPR2_SMP5_13   = (0b010 << 15);  /* channel 5 sample time: 13.5 cycles */
static const u32 adc_SMPR2_SMP5_28   = (0b011 << 15);  /* channel 5 sample time: 28.5 cycles */
static const u32 adc_SMPR2_SMP5_41   = (0b100 << 15);  /* channel 5 sample time: 41.5 cycles */
static const u32 adc_SMPR2_SMP5_55   = (0b101 << 15);  /* channel 5 sample time: 55.5 cycles */
static const u32 adc_SMPR2_SMP5_71   = (0b110 << 15);  /* channel 5 sample time: 71.5 cycles */
static const u32 adc_SMPR2_SMP5_239  = (0b111 << 15);  /* channel 5 sample time: 239.5 cycles */
static const u32 adc_SMPR2_SMP4_MASK = (0b111 << 12);
static const u32 adc_SMPR2_SMP4_1    = (0b000 << 12);  /* channel 4 sample time: 1.5 cycles */
static const u32 adc_SMPR2_SMP4_7    = (0b001 << 12);  /* channel 4 sample time: 7.5 cycles */
static const u32 adc_SMPR2_SMP4_13   = (0b010 << 12);  /* channel 4 sample time: 13.5 cycles */
static const u32 adc_SMPR2_SMP4_28   = (0b011 << 12);  /* channel 4 sample time: 28.5 cycles */
static const u32 adc_SMPR2_SMP4_41   = (0b100 << 12);  /* channel 4 sample time: 41.5 cycles */
static const u32 adc_SMPR2_SMP4_55   = (0b101 << 12);  /* channel 4 sample time: 55.5 cycles */
static const u32 adc_SMPR2_SMP4_71   = (0b110 << 12);  /* channel 4 sample time: 71.5 cycles */
static const u32 adc_SMPR2_SMP4_239  = (0b111 << 12);  /* channel 4 sample time: 239.5 cycles */
static const u32 adc_SMPR2_SMP3_MASK = (0b111 << 9);
static const u32 adc_SMPR2_SMP3_1    = (0b000 << 9);  /* channel 3 sample time: 1.5 cycles */
static const u32 adc_SMPR2_SMP3_7    = (0b001 << 9);  /* channel 3 sample time: 7.5 cycles */
static const u32 adc_SMPR2_SMP3_13   = (0b010 << 9);  /* channel 3 sample time: 13.5 cycles */
static const u32 adc_SMPR2_SMP3_28   = (0b011 << 9);  /* channel 3 sample time: 28.5 cycles */
static const u32 adc_SMPR2_SMP3_41   = (0b100 << 9);  /* channel 3 sample time: 41.5 cycles */
static const u32 adc_SMPR2_SMP3_55   = (0b101 << 9);  /* channel 3 sample time: 55.5 cycles */
static const u32 adc_SMPR2_SMP3_71   = (0b110 << 9);  /* channel 3 sample time: 71.5 cycles */
static const u32 adc_SMPR2_SMP3_239  = (0b111 << 9);  /* channel 3 sample time: 239.5 cycles */
static const u32 adc_SMPR2_SMP2_MASK = (0b111 << 6);
static const u32 adc_SMPR2_SMP2_1    = (0b000 << 6);  /* channel 2 sample time: 1.5 cycles */
static const u32 adc_SMPR2_SMP2_7    = (0b001 << 6);  /* channel 2 sample time: 7.5 cycles */
static const u32 adc_SMPR2_SMP2_13   = (0b010 << 6);  /* channel 2 sample time: 13.5 cycles */
static const u32 adc_SMPR2_SMP2_28   = (0b011 << 6);  /* channel 2 sample time: 28.5 cycles */
static const u32 adc_SMPR2_SMP2_41   = (0b100 << 6);  /* channel 2 sample time: 41.5 cycles */
static const u32 adc_SMPR2_SMP2_55   = (0b101 << 6);  /* channel 2 sample time: 55.5 cycles */
static const u32 adc_SMPR2_SMP2_71   = (0b110 << 6);  /* channel 2 sample time: 71.5 cycles */
static const u32 adc_SMPR2_SMP2_239  = (0b111 << 6);  /* channel 2 sample time: 239.5 cycles */
static const u32 adc_SMPR2_SMP1_MASK = (0b111 << 3);
static const u32 adc_SMPR2_SMP1_1    = (0b000 << 3);  /* channel 1 sample time: 1.5 cycles */
static const u32 adc_SMPR2_SMP1_7    = (0b001 << 3);  /* channel 1 sample time: 7.5 cycles */
static const u32 adc_SMPR2_SMP1_13   = (0b010 << 3);  /* channel 1 sample time: 13.5 cycles */
static const u32 adc_SMPR2_SMP1_28   = (0b011 << 3);  /* channel 1 sample time: 28.5 cycles */
static const u32 adc_SMPR2_SMP1_41   = (0b100 << 3);  /* channel 1 sample time: 41.5 cycles */
static const u32 adc_SMPR2_SMP1_55   = (0b101 << 3);  /* channel 1 sample time: 55.5 cycles */
static const u32 adc_SMPR2_SMP1_71   = (0b110 << 3);  /* channel 1 sample time: 71.5 cycles */
static const u32 adc_SMPR2_SMP1_239  = (0b111 << 3);  /* channel 1 sample time: 239.5 cycles */
static const u32 adc_SMPR2_SMP0_MASK = (0b111 << 0);
static const u32 adc_SMPR2_SMP0_1    = (0b000 << 0);  /* channel 0 sample time: 1.5 cycles */
static const u32 adc_SMPR2_SMP0_7    = (0b001 << 0);  /* channel 0 sample time: 7.5 cycles */
static const u32 adc_SMPR2_SMP0_13   = (0b010 << 0);  /* channel 0 sample time: 13.5 cycles */
static const u32 adc_SMPR2_SMP0_28   = (0b011 << 0);  /* channel 0 sample time: 28.5 cycles */
static const u32 adc_SMPR2_SMP0_41   = (0b100 << 0);  /* channel 0 sample time: 41.5 cycles */
static const u32 adc_SMPR2_SMP0_55   = (0b101 << 0);  /* channel 0 sample time: 55.5 cycles */
static const u32 adc_SMPR2_SMP0_71   = (0b110 << 0);  /* channel 0 sample time: 71.5 cycles */
static const u32 adc_SMPR2_SMP0_239  = (0b111 << 0);  /* channel 0 sample time: 239.5 cycles */

/* RM0008 table 3 */
#if defined(FEATURE_ADC1)
static struct adc * const ADC1 = (void *)0x40012400;
#endif
#if defined(FEATURE_ADC2)
static struct adc * const ADC2 = (void *)0x40012800;
#endif
#if defined(FEATURE_ADC3)
static struct adc * const ADC3 = (void *)0x40013C00;
#endif

#endif

