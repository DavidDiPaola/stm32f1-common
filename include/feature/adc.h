/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#ifndef _COMMON_FEATURE_ADC_H
#define _COMMON_FEATURE_ADC_H

/* RM0008 figure 1, figure 2 */
#define FEATURE_ADC1 1

/* RM0008 figure 1, figure 2 */
#define FEATURE_ADC2 1

/* RM0008 figure 1, figure 2 */
#if (defined(MCU_LINE_F101) || defined(MCU_LINE_F103))
	#define FEATURE_ADC3 1
#endif

#endif

